<html>
   <head>
      <title>Vue JS Demo</title>
      <script type = "text/javascript" src = "https://cdnjs.cloudflare.com/ajax/libs/vue/2.4.0/vue.js"></script>
   </head>
   <body>
      <div id="demo">
         <h1>{{ message }}</h1>
      </div>
      <div id = "vue_det">
         <h1>Firstname : {{firstname}}</h1>
         <h1>Lastname : {{lastname}}</h1>
         <h1>{{ mydetails() }}</h1>
      </div>
      <div id="try">
         <p>{{ fname }} {{ lname }}</p>
      </div>
      <div id="pro"></div>
      <script type="text/javascript">
         var firstdemo = new Vue({
            el : '#demo',
            data: {
               message: 'Hey, It is my first demo in vue js'
            }
         });
         // print message
         var name = new Vue({
            el: '#vue_det',
            data: {
               firstname: 'Devanshi',
               lastname: 'Dave'
            },
            methods: {
               mydetails : function() {
                  return "I am "+this.firstname +" "+ this.lastname;
               }
            }
         });
         // print message using method
          var obj = { fname: "Raj", lname: "Patel"}
          var vm = new Vue({
            el:'#try',
            data: obj
         });
          var Component = Vue.extend({
            data: function () {
               return obj
            }
         });
          //vue.extend use
         var myComponentInstance = new Component();
         console.log(myComponentInstance.lname);
         console.log(myComponentInstance.$data);
         console.log(vm.fname)+console.log(vm.lname);
         console.log(vm.$data);
         console.log(vm.$data.fname);
         // using prop
         var Comp = Vue.extend({
            
            props: ['msg'],
            template: '<div><h1>{{ msg }}</h1></div>'
         });
         var vmm = new Comp({
            el:"#pro",
            propsData: {
               msg: 'hello'
            }  
         });
      </script>
   </body>
</html>

<!-- instance of vue -->
<html>
   <head>
      <title>VueJs Introduction</title>
      <script type = "text/javascript" src = "https://cdnjs.cloudflare.com/ajax/libs/vue/2.4.0/vue.js"></script>
   </head>
   <body>
      <script type = "text/javascript">
         var vm = new Vue({
            data: { a: 2 },
            computed: {
            
               // get only, just need a function
               aSum: function () {
                  return this.a + 2;
               },
               
               // both get and set
               aSquare: {
                  get: function () {
                     return this.a*this.a;
                  },
                  set: function (v) {
                     this.a = v*2;
                  }
               }
            }
         })
         console.log(vm.aSum); // -> 4
         console.log(vm.aSquare);  // -> 4
         vm.aSquare = 3;
         console.log(vm.a);       // -> 6
         console.log(vm.aSum); // -> 8
      </script>
      <script type = "text/javascript">
         var vms = new Vue({  
            data: { a: 5 },
            methods: {
               asquare: function () {
                  this.a *= this.a;
               }
            }
         })
         vms.asquare();
         console.log(vms.a);
      </script>
   </body>
</html>


<!-- html content using v-html -->

<html>
   <head>
      <title>VueJs Instance</title>
      <script type = "text/javascript" src = "https://cdnjs.cloudflare.com/ajax/libs/vue/2.4.0/vue.js"></script>
   </head>
   <body>
      <div id = "vue_dets">
         <h1>Firstname : {{firstname}}</h1>
         <h1>Lastname : {{lastname}}</h1>
         <div v-html = "htmlcontent"></div>
      </div>
      <script type = "text/javascript">
         var vm = new Vue({
         el: '#vue_dets',
         data: {
            firstname : "Ria",
            lastname  : "Singh",
            htmlcontent : "<h1>Vue Js Template</h1> "
         }
      });
      </script>
   </body>
</html>

