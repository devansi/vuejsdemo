<html>
   <head>
      <title>VueJs Instance</title>
      <script type = "text/javascript" src = "https://cdnjs.cloudflare.com/ajax/libs/vue/2.4.0/vue.js"></script>
   </head>
   <body>
      <div id = "computed_props">
         FirstName : <input type = "text" v-model = "firstname" /> <br/><br/>
         LastName : <input type = "text" v-model = "lastname"/> <br/><br/>
         Birthyear : <input type = "text" v-model = "birthyear"/> <br/><br/>
         <h1>My name is {{firstname}} {{lastname}} {{birthyear}}</h1>
         <h1>Using computed method : {{getfullname}}</h1>
      </div>
      <script type = "text/javascript">
         var vm = new Vue({
         el: '#computed_props',
         data: {
            firstname :"",
            lastname :"",
            birthyear : ""
         },
         computed :{
            getfullname : function(){
               return this.firstname +" "+ this.lastname +" "+ this.birthyear;
            }
         }
      })
      </script>
   </body>
</html>



<html>
   <head>
      <title>VueJs Instance</title>
      <script type = "text/javascript" src = "https://cdnjs.cloudflare.com/ajax/libs/vue/2.4.0/vue.js"></script>
   </head>
   <body>
      <div id = "computed_prop">
         <input type = "text" v-model = "fullname" />
         <h1>{{firstName}}</h1>
         <h1>{{lastName}}</h1>
      </div>
      <script type = "text/javascript">
         var vm = new Vue({
            el: '#computed_prop',
            data: {
               firstName : "Terry",
               lastName : "Ben"
            },
            computed :{
               fullname : {
                  get : function() {
                     return this.firstName+" "+this.lastName;
                  }
               }
            }
         });
      </script>
   </body>
</html>


<html>
   <head>
      <title>VueJs Instance</title>
      <script type = "text/javascript" src = "js/vue.js"></script>
   </head>
   <body>
      <div id = "computed_propss">
         <input type = "text" v-model = "fullname" />
         <h1>{{firstName}}</h1>
         <h1>{{lastName}}</h1>
      </div>
      <script type = "text/javascript">
         var vm = new Vue({
            el: '#computed_propss',
            data: {
               firstName : "Terry",
               lastName : "Ben"
            },
            methods: {
            },
            computed :{
               fullname : {
                  get : function() {
                     return this.firstName+" "+this.lastName;
                  },
                  set : function(name) {
                     var fname = name.split(" ");
                     this.firstName = fname[0];
                     this.lastName = fname[1]
                  }
               }
            }
         });
      </script>
   </body>
</html>