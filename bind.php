<html>
   <head>
      <title>VueJs Instance</title>
      <script type = "text/javascript" src = "https://cdnjs.cloudflare.com/ajax/libs/vue/2.4.0/vue.js"></script>
   </head>
   <body>
       <style>
         .active {
            background: red;
         }
      </style>
      <div id = "databinding">
         {{title}}<br/>
         <a href = "hreflink" target = "_blank"> Click Me </a> <br/>
         <a href = "{{hreflink}}" target = "_blank">Click Me </a>  <br/>
         <a v-bind:href = "hreflink" v-bind:class = "{active:isactive}" target = "_blank">Click Me </a>   <br/>
         <a :href = "hreflink" target = "_blank">Click Me </a> <br/>
          <img v-bind:src = "imgsrc" width = "300" height = "250" /> <br/>
          <new_component class = "active"></new_component><br>
          <new_component v-bind:class = "{active:isActive}"></new_component>
         
      </div>
      <script type = "text/javascript">
         var vm = new Vue({
            el: '#databinding',
            data: {
               title : "DATA BINDING",
               hreflink : "http://www.google.com",
               isactive : true,
               imgsrc : "images/bgimg1.jpg",
               
            },
            components:{
               'new_component' : {
                  template : '<div class = "info">Class Binding for component</div>'
               }
            }
            
         });
      </script>
   </body>
</html>

<!-- class binding -->

<html>
   <head>
      <title>VueJs Instance</title>
      <script type = "text/javascript" src = "https://cdnjs.cloudflare.com/ajax/libs/vue/2.4.0/vue.js"></script>
   </head>
   <body>
      <style>
         .active {
            background: red;
         }
      </style>
      <div id = "classbinding">
         <div v-bind:class = "{active:isactive}"><b>{{title}}</b></div>
      </div>
      <script type = "text/javascript">
         var vm = new Vue({
            el: '#classbinding',
            data: {
               title : "CLASS BINDING",
               isactive : true,
              
            }
         });
      </script>
   </body>
</html>


<!-- instance -->

<html>
   <head>
      <title>VueJs Instance</title>
      <script type = "text/javascript" src = "https://cdnjs.cloudflare.com/ajax/libs/vue/2.4.0/vue.js"></script>
   </head>
   <body>
      <style>
         .info {
            color: #00529B;
            background-color: #BDE5F8;
         }
         div {
            margin: 10px 0;
            padding: 12px;
         }
         .actives {
            color: #4F8A10;
            background-color: #DFF2BF;
         }
         .displayError{
            color: #D8000C;
            background-color: #FFBABA;
         }
      </style>
      <div id = "classbindinginfo">
         <div class = "info"  v-bind:class = "{ actives: isActive, 'displayError': hasError }">
            {{title}}
            <br>
         </div>
         <new_component class = "active"></new_component>
      </div>
      <script type = "text/javascript">
         var vm = new Vue({
            el: '#classbindinginfo',
            data: {
               title : "This is class binding example",
               isActive : false,
               hasError : false
            },
            components:{
               'new_component' : {
                  template : '<div class = "actives">Class Binding for component</div>'
               }
            }
         });
      </script>
   </body>
</html>

<!-- bind style -->

<html>
   <head>
      <title>VueJs Instance</title>
      <script type = "text/javascript" src = "https://cdnjs.cloudflare.com/ajax/libs/vue/2.4.0/vue.js"></script>
   </head>
   <body>
      <div id = "databindingaa">
         <div v-bind:style = "{ color: activeColor, fontSize: fontSize  }">{{titles}}</div>
      </div>
      <script type = "text/javascript">
         var vm = new Vue({
            el: '#databindingaa',
            data: {
               titles : "Inline style Binding",
               activeColor: 'blue',
               fontSize :'30px'
            }
         });
      </script>
   </body>
</html>


<!-- v-model binding -->

<html>
   <head>
      <title>VueJs Instance</title>
      <script type = "text/javascript" src = "https://cdnjs.cloudflare.com/ajax/libs/vue/2.4.0/vue.js"></script>
   </head>
   <body>
      <div id = "binding">
         <h3>TEXTBOX</h3>
         <input  v-model = "name" placeholder = "Enter Name" />
         <h3>Name entered is : {{name}}</h3>
         <hr/>
         <h3>Textarea</h3>
         <textarea v-model = "textmessage" placeholder = "Add Details"></textarea>
         <h1><p>{{textmessage}}</p></h1>
         <hr/>
         <h3>Checkbox</h3>
         <input type = "checkbox" id = "checkbox" v-model = "checked"> {{checked}}
         <hr>
         <h3>Radio</h3>
         <input type = "radio" id = "black" value = "Black" v-model = "picked">Black
         <input type = "radio" id = "white" value = "White" v-model = "picked">White
         <h3>Radio element clicked : {{picked}} </h3>
         <hr/>
         <h3>Select</h3>
         <select v-model = "languages">
            <option disabled value = "">Please select one</option>
            <option>Java</option>
            <option>Javascript</option>
            <option>Php</option>
            <option>C</option>
            <option>C++</option>
         </select>
         <h3>Languages Selected is : {{ languages }}</h3>
         <hr/>
         <!-- Number modifier allows to only enter numbers. It will not take any other input besides numbers. -->
         <span style = "font-size:25px;">Enter Age:</span> <input v-model.number = "age" type = "number">
         <br/>
         <!-- Lazy modifier will display the content present in the textbox once it is fully entered and the user leaves the textbox. -->
         <span style = "font-size:25px;">Enter Message:</span> <input v-model.lazy = "msg">
         <h3>Display Message : {{msg}} Display Age : {{age}}</h3>
         <br/>
         <!-- Trim modifier will remove any spaces entered at the start and at the end. -->
         <span style = "font-size:25px;">Enter Message : </span><input v-model.trim = "message">
         <h3>Display Message : {{message}}</h3>
      </div>
      <script type = "text/javascript">
         var vm = new Vue({
            el: '#binding',
            data: {
               name:'',
               textmessage:'',
               checked : true,
                picked : 'White',
               languages : "Java",
               age : 0,
               msg: '',
               message : ''
            }
         });
      </script>
   </body>
</html>