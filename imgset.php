<html>
   <head>
      <title>VueJs Instance</title>
      <script type = "text/javascript" src = "https://cdnjs.cloudflare.com/ajax/libs/vue/2.4.0/vue.js"></script>
   </head>
   <body>
      <div id = "vue_det">
         <h1>Firstname : {{firstname}}</h1>
         <h1>Lastname : {{lastname}}</h1>
         <div v-html = "htmlcontent"></div>
         <!-- v-bind:src is user for set the img using Vue --> 
         <img v-bind:src = "imgsrc" width = "300" height = "250" />
      </div>
      <script type = "text/javascript">
         var vm = new Vue({
         el: '#vue_det',
         data: {
            firstname : "Ria",
            lastname  : "Singh",
            htmlcontent : "<h1>Vue Js Template</h1><table style='border:1px solid #000;'><tr><td>Devu</td><td>Jay</td></tr><td>1</td><td>2</td><tr></tr></table>",
            imgsrc : "images/bgimg1.jpg"
         }
      })
      </script>
   </body>
</html>