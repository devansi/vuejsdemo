<html>
   <head>
      <title>VueJs Instance</title>
      <script type = "text/javascript" src = "https://cdnjs.cloudflare.com/ajax/libs/vue/2.4.0/vue.js"></script>
   </head>
   <body>
      <div id = "databinding">
         <button v-on:click = "showdata" v-bind:style = "styleobj">Click Me</button>
         <span style = "font-size:25px;"><b>{{show}}</b></span>
         <h1 v-if = "show">This is h1 tag if part</h1>
         <h2 v-else>This is h2 tag else part</h2>
         <div v-show = "show">
            <b>V-Show:</b>
            <img src = "images/bgimg1.jpg" width = "100" height = "100" />
         </div>
      </div>
      <script type = "text/javascript">
         var vm = new Vue({
            el: '#databinding',
            data: {
               show: true,
               styleobj: {
                  backgroundColor: '#2196F3!important',
                  cursor: 'pointer',
                  padding: '8px 16px',
                  verticalAlign: 'middle',
               }
            },
            methods : {
               showdata : function() {
                  this.show = !this.show;
               }
            },
         });
      </script>
   </body>
</html>


<!-- v-for list rendering -->

<html>
   <head>
      <title>VueJs Instance</title>
      <script type = "text/javascript" ></script>
   </head>
   <body>
      <div id = "databindingrendering">
         <input type = "text" v-on:keyup.enter = "showinputvalue"
            v-bind:style = "styleobj" placeholder = "Enter Fruits Names"/>
         <h1 v-if = "items.length>0">Display Fruits Name</h1>
         <ul>
            <li v-for = "(a,index) in items">{{index}}..{{a}}</li>
         </ul>
      </div>
      <script type = "text/javascript">
         var vm = new Vue({
            el: '#databindingrendering',
            data: {
               items:[],
               styleobj: {
                  width: "30%",
                  padding: "12px 20px",
                  margin: "8px 0",
                  boxSizing: "border-box"
               }
            },
            methods : {
               showinputvalue : function(event) {
                  this.items.push(event.target.value);
               }
            },
         });
      </script>
   </body>
</html>